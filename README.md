# User guide

### How to run the tests
 * There are two plugins added to the pom.xml file, one is the surfire plugin 
and the other is the failsafe plugin. The surefire plugin is used to run the 
   unit tests, while the failsafe plugin is used to run the integration tests.
 * To run only the unit tests, you can use 'mvn clean test'
 * To run both the unit and integration test, use 'mvn clean install'
