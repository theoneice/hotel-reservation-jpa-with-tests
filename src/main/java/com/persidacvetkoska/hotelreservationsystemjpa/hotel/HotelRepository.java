package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

public interface HotelRepository extends JpaRepository<Hotel, Integer>, JpaSpecificationExecutor<Hotel> {

    Optional<Hotel> findByNameIgnoreCaseAndLocationIgnoreCase(String name, String location);

    @Query(nativeQuery = true, value = "select coalesce(sum(number_guests), 0) from reservation where hotel_id = :hotelId and start_date <= :onDate and end_date > :onDate")
    Integer getNumberGuests(Integer hotelId, LocalDate onDate);

    @Query(nativeQuery = true, value = "select coalesce(count(*), 0) from reservation " +
                                       "where hotel_id = :hotelId and start_date <= :onDate " +
                                        "and end_date > :onDate and status <> 'Canceled'")
    Integer getNumberReservedRooms(Integer hotelId, LocalDate onDate);

    @Query(nativeQuery = true, value = "select coalesce(count(*), 0) from room " +
                                        "where hotel_id = :hotelId " +
                                        "and not exists (select 1 " +
                                                         "from reservation " +
                                                         "where hotel_id = :hotelId " +
                                                         "and room_id = room.id " +
                                                         "and start_date <= :onDate " +
                                                         "and end_date > :onDate " +
                                                         "and status <> 'Canceled')")
    Integer getNumberFreeRooms(Integer hotelId, LocalDate onDate);

    @Query(nativeQuery = true, value = "select sum(room.price)\n" +
            "from reservation \n" +
            "inner join room on reservation.room_id = room.id\n" +
            "where reservation.hotel_id = :hotelId\n" +
            "and status <> 'Canceled'\n" +
            "and start_date <= :onDate\n" +
            "and end_date > :onDate")
    BigDecimal getProfitForPeriod(Integer hotelId, LocalDate onDate);
    
}
