package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceValidationException;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.RoomRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.util.ReturnValueDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HotelService {

    private final HotelRepository repository;
    private final RoomRepository roomRepository;

    public HotelDto create(final HotelRequest request) {
        var isPresentHotel = repository.findByNameIgnoreCaseAndLocationIgnoreCase(request.name, request.location).isPresent();
        if (isPresentHotel){
            throw new ResourceValidationException("Hotel already exists");
        }
        var hotel = new Hotel(request.name, request.location);
        return repository.save(hotel).toDto();
//        var hotel = HotelMapper.fromRequest(request);
//        var createdHotel = repository.save(hotel);
//        return HotelMapper.fromEntity(createdHotel);
    }

    public HotelDto update (final Integer id, final HotelRequest request){

        var foundHotel = repository.findById(id);
        Hotel currentHotel = foundHotel.orElseThrow(() -> new ResourceNotFoundException("Hotel with id = " + id + " not found!") );
        currentHotel.name = request.name;
        currentHotel.location = request.location;
        var updatedHotel = repository.save(currentHotel);
        return updatedHotel.toDto();
    }

    public HotelDto findHotelById(Integer id){
        var foundHotel = repository.findById(id);
        return foundHotel
                .orElseThrow(() -> new ResourceNotFoundException("Hotel with id = " + id + " not found!"))
                .toDto();
    }

    public Page<HotelDto> findPage(Pageable page){
        var foundHotels = repository.findAll(page);
        return foundHotels.map(hotel -> hotel.toDto());
    }

    public Page<HotelDto> findPageByNameAndLocation(HotelSearchRequest request){
        var foundHotels = repository.findAll(request.generateSpecification(), request.pageable);
        return foundHotels.map(hotel -> hotel.toDto());
    }

    public ReturnValueDto getNumberGuests(Integer hotelId, LocalDate onDate){
        Integer numGuests = repository.getNumberGuests(hotelId, onDate);
        return new ReturnValueDto(numGuests.toString());
    }

    public ReturnValueDto getNumberReservedRooms(Integer hotelId, LocalDate onDate){
        Integer numReservedRooms = repository.getNumberReservedRooms(hotelId, onDate);
        return new ReturnValueDto(numReservedRooms.toString());
    }

    public ReturnValueDto getNumberFreeRooms(Integer hotelId, LocalDate onDate){
        Integer numFreeRooms = repository.getNumberFreeRooms(hotelId, onDate);
        return new ReturnValueDto(numFreeRooms.toString());
    }

    public ReturnValueDto getProfitForPeriod(Integer hotelId, LocalDate onDate){
        BigDecimal profit = repository.getProfitForPeriod(hotelId, onDate);
        return new ReturnValueDto(profit.toString());
    }

    public void deleteHotelById(Integer id){
        var foundHotel = repository.findById(id);
        var deleteHotel = foundHotel.orElseThrow(() -> new ResourceNotFoundException("Hotel with id = " + id + " not found!"));
        var foundRoom = roomRepository.findFirstByHotel(deleteHotel);
        if (foundRoom.isPresent()){
            throw new ResourceValidationException("Hotel has rooms and can't be deleted!");
        }
        repository.delete(deleteHotel);
    }
}
