package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import org.springframework.data.jpa.domain.Specification;


public class HotelSpecifications {

    private HotelSpecifications(){}

    public static Specification<Hotel> byNameLiteralEquals(final String name){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")),
                name.toLowerCase()));
    }

    public static Specification<Hotel> byLocationLiteralEquals(final String location){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("location")),
                location.toLowerCase()));
    }
}
