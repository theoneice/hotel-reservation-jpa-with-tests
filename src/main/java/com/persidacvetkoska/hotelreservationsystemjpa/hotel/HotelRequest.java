package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import javax.validation.constraints.Size;

public class HotelRequest {

    @Size(max = 50, message = "Name must be up to 50 characters long")
    public String name;
    @Size(max = 50, message = "Location must be up to 50 characters long")
    public String location;

    public HotelRequest(String name, String location) {
        this.name = name;
        this.location = location;
    }

}
