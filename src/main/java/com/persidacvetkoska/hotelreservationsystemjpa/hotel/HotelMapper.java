package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

// SHOULD NOT BE USED IF POSSIBLE !!!
public final class HotelMapper {

    private HotelMapper() {}

    public static Hotel fromRequest(final HotelRequest request) {
        return new Hotel(request.name, request.location);
    }

    public static HotelDto fromEntity(final Hotel entity) {
        return entity.toDto();
    }
}
