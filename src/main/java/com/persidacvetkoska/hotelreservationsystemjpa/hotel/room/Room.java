package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.persidacvetkoska.hotelreservationsystemjpa.hotel.Hotel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class Room {
    @Id
    @GeneratedValue
    public Integer id;
    @Column(name = "room_number")
    public String roomNumber;
    public String type;
    @Column(name = "number_persons")
    public Integer numberOfPersons;
    public BigDecimal price;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    public Hotel hotel;

    public Room() {
    }

    public Room(String roomNumber, String type, Integer numberOfPersons, BigDecimal price, Hotel hotel) {
        this.roomNumber = roomNumber;
        this.type = type;
        this.numberOfPersons = numberOfPersons;
        this.price = price;
        this.hotel = hotel;
    }

    public Room(Integer id, String roomNumber, String type, Integer numberOfPersons, BigDecimal price, Hotel hotel) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.type = type;
        this.numberOfPersons = numberOfPersons;
        this.price = price;
        this.hotel = hotel;
    }

    public RoomDto toDto(){
        return new RoomDto(id, roomNumber, type, numberOfPersons, price, hotel.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(id, room.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
