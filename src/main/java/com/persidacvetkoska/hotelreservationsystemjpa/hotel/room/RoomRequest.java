package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.RoomTypeNotValidException;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RoomRequest {
    @Size(max = 4, message = "Room number is up to 4 characters")
    @Pattern(regexp="^([1-9][0-9]*)$", message = "Room number consist only of numbers!")
    public String roomNumber;
    public RoomType type;

    public RoomRequest(String roomNumber, String type, Integer hotelId) {
        this.roomNumber = roomNumber;
        if (RoomType.valueOfType(type) == null){
            throw new RoomTypeNotValidException("Room type is not valid!");
        }
        this.type = RoomType.valueOfType(type);
    }
}
