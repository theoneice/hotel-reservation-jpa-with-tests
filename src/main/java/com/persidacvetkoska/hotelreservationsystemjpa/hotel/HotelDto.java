package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode
public class HotelDto {
    public Integer id;
    public String name;
    public String location;

    public HotelDto(Integer id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public HotelDto(String name, String location) {
        this.name = name;
        this.location = location;
    }
}
