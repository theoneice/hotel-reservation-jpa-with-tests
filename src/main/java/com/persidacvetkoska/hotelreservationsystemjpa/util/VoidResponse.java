package com.persidacvetkoska.hotelreservationsystemjpa.util;

public class VoidResponse {

    public boolean success;

    public VoidResponse(boolean success){
        this.success = success;
    }
}
