package com.persidacvetkoska.hotelreservationsystemjpa.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(FIELD)
@Constraint(validatedBy = AgeValidator.class)
@Retention(RUNTIME)
@Documented
public @interface ValidAge {
    String message() default "Guest must be older than 18";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
