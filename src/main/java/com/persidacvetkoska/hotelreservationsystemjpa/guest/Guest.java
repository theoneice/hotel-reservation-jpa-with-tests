package com.persidacvetkoska.hotelreservationsystemjpa.guest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Guest {
    @Id
    @GeneratedValue
    public Integer id;
    @Column(name = "first_name")
    public String firstName;
    @Column(name = "last_name")
    public String lastName;
    @Column(name = "date_of_birth")
    public LocalDate dateOfBirth;
    @Column(name = "address")
    public String address;
    @Column(name = "city")
    public String city;
    @Column(name = "country")
    public String country;
    @Column(name = "email")
    public String email;
    @Column(name = "phone_number")
    public String phoneNumber;

    public Guest() {
    }

    public Guest(Integer id, String firstName, String lastName, LocalDate dateOfBirth, String address, String city, String country, String email, String phoneNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.city = city;
        this.country = country;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Guest(String firstName, String lastName, LocalDate dateOfBirth, String address, String city, String country, String email, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.city = city;
        this.country = country;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public GuestDto toDto(){
        return new GuestDto(this.id, this.firstName, this.lastName, this.dateOfBirth, this.address, this.city, this.country, this.email, this.phoneNumber);
    }
}
