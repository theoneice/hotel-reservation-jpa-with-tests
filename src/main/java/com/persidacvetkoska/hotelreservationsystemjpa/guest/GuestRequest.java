package com.persidacvetkoska.hotelreservationsystemjpa.guest;


import com.persidacvetkoska.hotelreservationsystemjpa.util.ValidAge;
import lombok.AllArgsConstructor;
import javax.validation.constraints.*;
import java.time.LocalDate;

@AllArgsConstructor
public class GuestRequest {

    @NotBlank(message = "First name is mandatory!")
    @Pattern(regexp = "[a-z-A-Z]*", message = "First name has invalid characters")
    public String firstName;
    @NotBlank(message = "Last name is mandatory!")
    @Pattern(regexp = "[a-z-A-Z]*", message = "Last name has invalid characters")
    public String lastName;
    @Past(message = "Date of birth must be in the past")
    @ValidAge(message = "The guest must be older than 18!")
    public LocalDate dateOfBirth;
    @NotBlank(message = "Address is mandatory!")
    public String address;
    @NotBlank(message = "City is mandatory!")
    @Pattern(regexp = "[a-z-A-Z]*", message = "City has invalid characters")
    public String city;
    @NotBlank(message = "Country is mandatory!")
    @Pattern(regexp = "[a-z-A-Z]*", message = "Country has invalid characters")
    public String country;
    @NotBlank(message = "E-mail is mandatory!")
    @Email(message = "The email format is not valid!")
    public String email;
    @Pattern(regexp = "[0-9]*", message = "Phone number has invalid characters")
    public String phoneNumber;

    public Guest toEntity(){
        return new Guest(this.firstName, this.lastName, this.dateOfBirth, this.address, this.city, this.country, this.email, this.phoneNumber);
    }

}
