package com.persidacvetkoska.hotelreservationsystemjpa.exception;

public class RoomTypeNotValidException extends RuntimeException{
    public RoomTypeNotValidException(String message) {
        super(message);
    }

    public RoomTypeNotValidException(String message, Throwable cause) {
        super(message, cause);
    }
}
