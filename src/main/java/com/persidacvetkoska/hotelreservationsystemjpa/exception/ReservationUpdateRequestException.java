package com.persidacvetkoska.hotelreservationsystemjpa.exception;

public class ReservationUpdateRequestException extends RuntimeException{
    public ReservationUpdateRequestException(String message) {
        super(message);
    }

    public ReservationUpdateRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
