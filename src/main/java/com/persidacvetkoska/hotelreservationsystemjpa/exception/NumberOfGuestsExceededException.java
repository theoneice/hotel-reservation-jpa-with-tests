package com.persidacvetkoska.hotelreservationsystemjpa.exception;

public class NumberOfGuestsExceededException extends RuntimeException{
    public NumberOfGuestsExceededException(String message) {
        super(message);
    }

    public NumberOfGuestsExceededException(String message, Throwable cause) {
        super(message, cause);
    }
}
