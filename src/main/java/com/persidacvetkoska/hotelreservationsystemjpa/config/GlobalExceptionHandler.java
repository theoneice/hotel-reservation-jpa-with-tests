package com.persidacvetkoska.hotelreservationsystemjpa.config;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.*;
import com.persidacvetkoska.hotelreservationsystemjpa.util.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@ControllerAdvice
    @Component
    public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(ResourceValidationException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }

        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(NumberOfGuestsExceededException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }

        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(ResourceNotFoundException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }

        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(RoomAlreadyReservedException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
    }

        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(ReservationUpdateRequestException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }

        @Override
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
            LOGGER.error(exception.getMessage(), exception);
            final List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
            Map<String, Set<String>> errorsMap =  fieldErrors.stream().collect(
                    Collectors.groupingBy(FieldError::getField,
                            Collectors.mapping(FieldError::getDefaultMessage, Collectors.toSet())
                    )
            );
            return new ResponseEntity(errorsMap.isEmpty()? exception:errorsMap, headers, status);
        }


        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.BAD_REQUEST)
        public ErrorDto handle(RoomTypeNotValidException exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }


        @ExceptionHandler
        @ResponseBody
        @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        public ErrorDto handle(Exception exception){
            LOGGER.error(exception.getMessage(), exception);
            return new ErrorDto(exception.getMessage());
        }

        @Override
        protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
            LOGGER.error(e.getMessage(), e);
            RoomTypeNotValidException exception = (RoomTypeNotValidException) e.getMostSpecificCause();
            return new ResponseEntity<>(new ErrorDto(exception.getMessage()), HttpStatus.BAD_REQUEST);
        }

}
