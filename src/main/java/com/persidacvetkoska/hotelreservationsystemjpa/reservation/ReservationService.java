package com.persidacvetkoska.hotelreservationsystemjpa.reservation;


import com.persidacvetkoska.hotelreservationsystemjpa.exception.*;
import com.persidacvetkoska.hotelreservationsystemjpa.guest.GuestRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.HotelRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.RoomRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;


@Service
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final GuestRepository guestRepository;
    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;
    private final Validator validator;

    public ReservationDto create(ReservationRequest request){
        var foundGuest = guestRepository.findById(request.guestId)
                 .orElseThrow(() -> new ResourceNotFoundException("Guest doesn't exist!"));

        var foundHotel = hotelRepository.findById(request.hotelId)
                    .orElseThrow(() -> new ResourceNotFoundException("Hotel doesn't exist"));

        var foundRoom = roomRepository.findByIdAndHotelId (request.roomId, request.hotelId)
                                        .orElseThrow(() -> new ResourceNotFoundException("Room doesn't exist"));

        LocalDate endDate = request.startDate.plusDays(request.numberOfDays);

        validator.checkNumberOfGuests(foundRoom, request.numberOfGuests);
        validator.checkRoomAvailability(foundRoom, request.startDate, endDate, reservationRepository);

        ReservationStatusEnum status = ReservationStatusEnum.Reserved;

        Reservation reservation = new Reservation(foundGuest,foundHotel, foundRoom, request.startDate, endDate, request.numberOfGuests, status);
        return reservationRepository.save(reservation).toDto();
    }

    public ReservationDto update(Integer id, ReservationUpdateRequest request){

        if (id != request.id){
            throw new ReservationUpdateRequestException("Request for reservation update!");
        }

        var foundReservation = reservationRepository.findById(request.id)
                .orElseThrow(() -> new ResourceNotFoundException("Reservation with id = " + request.id + "doesn't exist"));

        validator.processRoomAvailability(foundReservation, request, reservationRepository);
        foundReservation.startDate = request.startDate;
        foundReservation.endDate = request.startDate.plusDays(request.numberOfDays);

        validator.checkNumberOfGuests(foundReservation.room, request.numberOfGuests);
        foundReservation.numberGuests = request.numberOfGuests;

        foundReservation.status = ReservationStatusEnum.valueOf(request.status);

        return reservationRepository.save(foundReservation).toDto();
    }

    public ReservationDto findById(Integer id){
        return reservationRepository.findById(id)
                 .orElseThrow(() -> new ResourceNotFoundException("Reservation with id = " + id + " not found!"))
                 .toDto();
    }

    public ReservationDto cancel(Integer id){
        var foundReservation = reservationRepository.findById(id)
                .orElseThrow(() -> new ResourceValidationException("Reservation not found!"));
        if (foundReservation.startDate.isBefore(LocalDate.now().plusDays(3))){
            throw new ResourceValidationException("Reservation can be made at least three days before start of the reservation!");
        }
        foundReservation.status = ReservationStatusEnum.Canceled;
        return reservationRepository.save(foundReservation).toDto();
    }

    public Page<ReservationDto> findPage(ReservationSearchRequest request,Pageable pageable){

      return reservationRepository.findAll(request.generateSpecification(), pageable).map(reservation -> reservation.toDto());
    }

    public void deleteById(Integer id){
        var foundReservation = reservationRepository.findById(id)
                                        .orElseThrow(() -> new ResourceNotFoundException("Reservation not found!"));
        reservationRepository.delete(foundReservation);
    }

}
