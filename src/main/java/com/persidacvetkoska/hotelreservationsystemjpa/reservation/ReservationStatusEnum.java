package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

public enum ReservationStatusEnum {
    Reserved,
    CheckedIn,
    CheckedOut,
    Canceled
}
