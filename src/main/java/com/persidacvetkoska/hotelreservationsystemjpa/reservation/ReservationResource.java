package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import com.persidacvetkoska.hotelreservationsystemjpa.util.VoidResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RestController
@RequestMapping("/api/reservations")
@RequiredArgsConstructor
public class ReservationResource {

    private final ReservationService service;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDto create(@RequestBody @Valid final ReservationRequest request){
        return service.create(request);
    }

    @PutMapping(path = "/{id}", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDto update(@PathVariable Integer id, @RequestBody @Valid final ReservationUpdateRequest request){
        return service.update(id, request);
    }

    @PutMapping(path = "/{id}/cancel", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDto cancel(@PathVariable Integer id){
        return service.cancel(id);
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReservationDto findById(@PathVariable Integer id){
        return service.findById(id);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<ReservationDto> findPage(@RequestParam(required = false) String firstName,
                                         @RequestParam(required = false) String lastName,
                                         @RequestParam(required = false) String hotelName,
                                         @RequestParam(required = false) String hotelLocation,
                                         @RequestParam(required = false) String roomNumber,
                                         @RequestParam(required = false) LocalDate startDate,  Pageable pageable){
        return service.findPage(new ReservationSearchRequest(firstName, lastName, hotelName, hotelLocation, roomNumber, startDate), pageable);
    }

    @PostMapping(path = "/search", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<ReservationDto> findPagePost(@RequestBody final ReservationSearchRequest request, Pageable pageable){
        return service.findPage(request, pageable);
    }

    @DeleteMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public VoidResponse deleteById(@PathVariable Integer id){
        service.deleteById(id);
        return new VoidResponse(true);
    }
}
