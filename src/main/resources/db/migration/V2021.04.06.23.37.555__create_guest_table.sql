CREATE TABLE public.guest
(
    id integer,
    first_name character varying(50) not null,
    last_name character varying(50) not null,
    date_of_birth date not null,
    address character varying(100) not null,
    city character varying(50) not null,
    country character varying(50) not null,
    email character varying(30) not null,
    phone_number character varying(30),
    CONSTRAINT guest_pkey PRIMARY KEY (id)
)