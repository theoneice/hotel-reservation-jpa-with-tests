package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import com.persidacvetkoska.hotelreservationsystemjpa.BaseIT;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;

public class HotelResourceIT extends BaseIT {

    @Autowired
    private Flyway flyway;

    @BeforeEach
    public void databaseFixture() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void testCreateHotelWithCorrectCreateRequest() throws Exception {
        var request = new HotelRequest("name", "location");

        var responseRaw = mockMvc.perform(post("/api/hotels")
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON_VALUE)
//                .header("Authorization", "Bearer " + accessToken.getAccessToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var dto = mapper.readValue(responseRaw, HotelDto.class);

        assertNotNull(dto);
        assertNotNull(dto.id);
//        assertEquals(request.name, dto.name);
//        assertEquals(request.location, dto.location);
    }

    @Test
    public void testFindHotel() throws Exception {
        var request = new HotelRequest("name", "location");

        var responseRawCreated = mockMvc.perform(post("/api/hotels")
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON_VALUE)
//                .header("Authorization", "Bearer " + accessToken.getAccessToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var dtoCreated = mapper.readValue(responseRawCreated, HotelDto.class);

        assertNotNull(dtoCreated);
        assertNotNull(dtoCreated.id);

        var responseRawFound = mockMvc.perform(get("/api/hotels/" + dtoCreated.id))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var dtoFound = mapper.readValue(responseRawFound, HotelDto.class);
        assertNotNull(dtoFound);
        assertEquals(dtoCreated, dtoFound);
    }
}
