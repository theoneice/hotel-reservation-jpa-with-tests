package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceValidationException;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.RoomRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HotelServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(HotelServiceTest.class);

    @InjectMocks
    private HotelService hotelService;

    @Mock
    private HotelRepository repository;

    @Mock
    private RoomRepository roomRepository;

    @BeforeEach
    public void beforeEach() {
//        when(repository.save(any(Hotel.class)))
//                .thenAnswer(invocation -> invocation.getArgument(0, Hotel.class));
        LOG.info("Before each");
    }

    @AfterEach
    public void afterEach() {
        LOG.info("After each");
    }

    @AfterAll
    public static void afterAll() {
        LOG.info("After all");
    }

    @BeforeAll
    public static void beforeAll() {
        LOG.info("Before all");
    }

    // Happy path
    @Test
    public void testCreateHotelWithCorrectCreateRequestAndNonExistingHotel() {
//        initRepositorySave();
        when(repository.save(any(Hotel.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Hotel.class));
        var request = new HotelRequest("name", "location");
        var expectedResult = new Hotel(request.name, request.location).toDto();
        var result = hotelService.create(request);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCreateHotelWithExistingHotelShouldThrowException() {
        var request = new HotelRequest("name", "location");
        when(repository.findByNameIgnoreCaseAndLocationIgnoreCase(any(), any()))
                .thenReturn(Optional.of(new Hotel()));

        assertThrows(ResourceValidationException.class, () -> hotelService.create(request));
    }

//    private void initRepositorySave() {
//        when(repository.save(any(Hotel.class)))
//                .thenAnswer(invocation -> invocation.getArgument(0, Hotel.class));
//    }
}
